<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FCM extends Model
{
	protected $primaryKey = 'id';

	protected $hidden = ['created_at','updated_at', 'is_active'];

	protected $fillable = [
        'name'
    ];

	function send_fcm($token, $title, $body) {

		define( 'API_ACCESS_KEY', 'AIzaSyCGwZTLd0cD5adks2nbzoK50co4b_8CkYo' );
		define("GOOGLE_GCM_URL", "https://fcm.googleapis.com/fcm/send");

		$img_url = 'http://is3.mzstatic.com/image/thumb/Purple71/v4/60/a0/4d/60a04daa-c238-97f8-5659-ba880d5f049e/pr_source.png/100x100bb-85.png';
	    $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';

		$fields = array(
	        'registration_ids' => $token,
	        'notification' => array('title' => $title, 'body' => $body),
	    );

	    $headers = array(
	        'Authorization:key=' . API_ACCESS_KEY,
	        'Content-Type:application/json'
	    );
		$ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	    $result = curl_exec($ch);

	    curl_close($ch);

	}
}
