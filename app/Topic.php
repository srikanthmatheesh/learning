<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
	protected $primaryKey = 'id';

	protected $hidden = ['created_at','updated_at', 'is_active'];

	protected $fillable = [
        'name'
    ];
}
