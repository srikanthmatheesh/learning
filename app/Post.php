<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $primaryKey = 'id';

	protected $hidden = ['created_at','updated_at', 'is_active'];

	protected $fillable = [
        'title',
		'content'
    ];
	public function user() {

        return $this->belongsTo('App\User');

    }

}
