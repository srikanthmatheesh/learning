## About Laravel

To install use composer

`composer install`

## To Run Laravel

In the project directory run the following command

`php artisan serve`

## API Source

API source is available at the following location

`learning/routes/api.php`

## API documentation

API documentatio attached in the following URL

[https://documenter.getpostman.com/view/4533110/S11NNxFy](https://documenter.getpostman.com/view/4533110/S11NNxFy)