<?php

use Illuminate\Http\Request;
use App\FCM;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function () {

	Route::get('/', function () {
		echo "Home Screen";
	});

	Route::get('topics', function (Request $request) {

		$topics = App\Topic::where(['is_active' => true])->get(['id','name AS label']);

		return Response::json([
			'status' => "200",
			'error' => false,
			'results' => $topics
		]);

	});

	Route::post('create-post', function (Request $request) {

		$validator = Validator::make($request->all(), [
			'topic_id' => 'required',
			'auth_token' => 'required',
			'content' => 'required',
		],
		[]);

		if ( $validator->fails() ) {

			return Response::json([
				'status' => 400,
				'error' => true,
				'message' => "Invalid post missing fields"
			],400);

		} else {

			$auth_token = $request->auth_token;

			$user = App\User::where(['is_active' => true, 'auth_token' => $auth_token])->first();

			if (isset($user->id)) {

				$post = new App\Post;
				$post->user_id = $user->id;
				$post->topic_id = $request->topic_id;
				$post->content = $request->content;
				$post->is_active = 1;

				if( $post->save()) {
					$tokens = [];

					$fcm_users = App\User::where(['is_active' => true, 'topic_id' => $request->topic_id])->get();
					foreach($fcm_users as $fcm_user) {
						$tokens[] = $fcm_user->notification_token;
					}

					$msg = new FCM();
					$title = 'PSG Software Learning';
					$body = $request->content;
					$msg->send_fcm($tokens, $title, $body);

					return Response::json([
						'status' => 200,
						'error' => false,
						'message' => 'Success'
					], 201);

				} else {

					return Response::json([
						'status' => 400,
						'error' => true,
						'message' => 'Try after sometime'
					], 400);

				}
			} else {

				return Response::json([
					'status' => 404,
					'error' => true,
					'message' => "Invalid user"
				],400);

			}
		}

	});

	Route::post('posts', function (Request $request) {

		$auth_token = $request->auth_token;

		$user = App\User::where(['is_active' => true, 'auth_token' => $auth_token])->first();

		if (isset($user->topic_id)) {

			$posts = App\Post::where(['is_active' => true, 'topic_id' => $user->topic_id])->with('user')->paginate();
			return Response::json( $posts);

		} else {

			return Response::json([
				'status' => 404,
				'error' => true,
				'message' => "Invalid user"
			],400);

		}

	});

	Route::post('registration', function (Request $request) {

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'auth_token' => 'required',
			'notification_token' => 'required',
			'topic_id' => 'required'
		],
		[]);

		if ( $validator->fails() ) {

			return Response::json([
				'status' => 400,
				'error' => true,
				'message' => "Invalid registration"
			],400);

		} else {

			$user = new App\User;
			$user->name = $request->name;
			$user->auth_token = $request->auth_token;
			$user->notification_token = $request->notification_token;
			$user->topic_id = $request->topic_id;
			$user->is_active = 1;
			if( $user->save()) {
				return Response::json([
					'status' => 200,
					'error' => false,
					'message' => 'Success'
				], 201);
			} else {
				return Response::json([
					'status' => 400,
					'error' => true,
					'message' => 'Try after sometime'
				], 400);
			}
		}

	});

});
